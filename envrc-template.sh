#!/bin/bash

## This is a template for .envrc file. Copy this to a new file called `.envrc` and fill in the values.

## NOTE: This file is ignored by git.
##       The .envrc file is NOT synced to the Docker container, it is used for local development only

# Primary GCS bucket to sync from (NOTE: gs:// should NOT be included)
export UPSTREAM_BUCKET="your-bucket-name-goes-here"

# Sync from this folder inside the ${UPSTREAM_BUCKET}
export UPSTREAM_BUCKET_FOLDER="/images" # or whatever folder name you give to sync from

#######
####### Build and Push variables. The following are used in the buildling and publishing of the Docker image to a GCR or Artifact Registry.
#######     Note, gcr.io and/or artifact registry must be setup ahead of time and user running `build.sh` must have permissions set already.
#######

### Image will be named: ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/image-puller:latest  (or `v${VERSION}` if using specific version)

# Example Artifact Registry
export PROJECT_ID="your-project-id-goes-here"
export REPO_HOST="us-west1-docker.pkg.dev"
export REPO_FOLDER="some-folder-in-repo"

# Example gcr.io
# export PROJECT_ID="your-project-id-goes-here"
# export REPO_HOST="gcr.io"
# export REPO_FOLDER="some-folder-in-repo"

