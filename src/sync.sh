#!/bin/bash

## This will pull images from a given remote GCS bucket and folder (folder is optional) and push them to a given local folder, then serve them up with a local webserver.


## Requirements:
## Need to run forever, poll on changes to upstream
## Need an ENV variable for the upstream bucket
## Need an ENV variable for the local folder (mounted PVC)
## Need an ENV variable for GCS Secret

UPSTREAM_BUCKET=${UPSTREAM_BUCKET}                              # Required ENV variable
UPSTREAM_BUCKET_FOLDER=${UPSTREAM_BUCKET_FOLDER:-/sync-images}  # defaults to /sync-images if not defined
CACHE_FOLDER=/images                                            # hard-coded in the YAML configuration as a volume mount

###
### Runs a validation to ensure all configration is set
###
function validate_config() {
    if [[ ! -d "${CACHE_FOLDER}" ]]; then
        echo "Missing a mounted folder for caching images. Aborting."
        exit 1
    fi

    if [[ -z "${GOOGLE_APPLICATION_CREDENTIALS}" ]]; then
        echo "Missing a Google Cloud Service Account JSON file. Aborting."
        exit 1
    fi

    if [[ -z "${UPSTREAM_BUCKET}" ]]; then
        echo "Missing an upstream bucket name. Aborting."
        exit 1
    fi

    if [[ "${UPSTREAM_BUCKET}" =~ ^gs ]]; then
        echo "The UPSTREAM_BUCKET name should NOT include 'gs://'. Aborting."
        exit 1
    fi
}

function validate_state() {
    gcloud storage ls gs://${UPSTREAM_BUCKET}${UPSTREAM_BUCKET_FOLDER} &> /dev/null
    if [[ $? -ne 0 ]]; then
        echo "Unable to access the upstream bucket with gcloud storage ls. Most often, this is permission based with the given GOOGLE_APPLICATION_CREDENTIALS. Aborting."
        exit 1
    fi
}

function print_config() {
    echo "UPSTREAM_BUCKET: ${UPSTREAM_BUCKET}"
    echo "UPSTREAM_BUCKET_FOLDER: ${UPSTREAM_BUCKET_FOLDER}"
    echo "CACHE_FOLDER: ${CACHE_FOLDER}"
    echo "GOOGLE_APPLICATION_CREDENTIALS: ${GOOGLE_APPLICATION_CREDENTIALS}"
    # print the autnentication state of gcloud
    gcloud auth list
}

function auth_gcp() {
    gcloud auth login --cred-file=${GOOGLE_APPLICATION_CREDENTIALS} --quiet --no-user-output-enabled
    if [[ $? -ne 0 ]]; then
        echo "Unable to authenticate with gcloud. Aborting."
        exit 1
    fi
}

function sync_images() {
    # sync images from GCS to local cache
    gcloud storage rsync gs://${UPSTREAM_BUCKET}${UPSTREAM_BUCKET_FOLDER} ${CACHE_FOLDER} --recursive --delete-unmatched-destination-objects
}

# Validate configuraiton state
validate_config
# Authenticate gcloud to access GCS
auth_gcp
# Validate running state
validate_state

# Print the configuration
print_config

# Sync images from GCS to local cache
while true; do
    sync_images
    sleep 60
done