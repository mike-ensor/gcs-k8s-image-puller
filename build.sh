#!/bin/bash

VERSION="v$1"

APP="${2:-image-puller}"            # default to image-puller if not given a value
DOCKERFILE="Dockerfile"

if [[ -z "${PROJECT_ID}" ]]; then
    echo "PROJECT_ID is not set. Aborting."
    exit 1
fi

docker build -f ${DOCKERFILE} -t ${APP}:${VERSION} .
if [[ $? -ne 0 ]]; then
    echo "Cannot build docker"
    exit 1
fi
docker tag ${APP}:${VERSION} ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP}:${VERSION}
if [[ $? -ne 0 ]]; then
    echo "Cannot Tag Docker Build with version"
    exit 1
fi
docker tag ${APP}:${VERSION} ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP}:latest
if [[ $? -ne 0 ]]; then
    echo "Cannot Tag Docker Build with latest"
    exit 1
fi
docker push ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP}:${VERSION}
if [[ $? -ne 0 ]]; then
    echo "Cannot Push Version Tagged"
    exit 1
fi
docker push ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP}:latest
if [[ $? -ne 0 ]]; then
    echo "Cannot Push Latest Tagged"
    exit 1
fi