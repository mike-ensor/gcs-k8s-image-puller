# Overview

This project is intended to sync a series of GCS bucket objects and pulling them local to a K8s cluster in a pod supported by a PVC

This project was developed to pull and expose the GCS buckets contents via Nginx. The primary reason was to expose .qcow2 and other VM disks for KubeVirt.

## Building

`./build #` where # = version you want to deploy with

